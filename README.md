# Test Stagiare OVA

## Installation

1 - Installer Node.js: https://nodejs.org/en/download
2 - Installer node_modules et les dépendances: npm install

## Exécuter le back-end

node server.js

## Pour ouvrir le front-end

Ouvrer le fichier "index.html" dans une page web.
