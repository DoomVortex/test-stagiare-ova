const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3000;

// Connect to MongoDB
mongoose.connect('mongodb+srv://test:test@cluster0.ymwokji.mongodb.net/liste_epicerie', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(() => {
    console.log('Connected to MongoDB');
  })
  .catch(err => {
    console.error('Failed to connect to MongoDB:', err);
  });

// Create a schema for grocery items
const itemSchema = new mongoose.Schema({
  item: {
    type: String,
    required: true
  }
});

// Create a model based on the schema
const Item = mongoose.model('Item', itemSchema);

// Middleware to parse JSON request bodies
app.use(express.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    next();
  });

// Route to get the list of items
app.get('/liste_epicerie', (req, res) => {
  Item.find({})
    .then(items => {
      res.json({ items });
    })
    .catch(err => {
      console.error('Failed to retrieve items:', err);
      res.status(500).json({ error: 'Failed to retrieve items' });
    });
});

// Route to add an item to the list
app.post('/liste_epicerie', (req, res) => {
  const newItem = new Item({ item: req.body.item });
  newItem.save()
    .then(() => {
      res.json({ message: 'Item added successfully' });
    })
    .catch(err => {
      console.error('Failed to add item:', err);
      res.status(500).json({ error: 'Failed to add item' });
    });
});

// Route to remove an item from the list
app.delete('/liste_epicerie/:id', (req, res) => {
  const itemId = req.params.id;
  Item.findByIdAndRemove(itemId)
    .then(() => {
      res.json({ message: 'Item removed successfully' });
    })
    .catch(err => {
      console.error('Failed to remove item:', err);
      res.status(500).json({ error: 'Failed to remove item' });
    });
});

// Start the server
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
